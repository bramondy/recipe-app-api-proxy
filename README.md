# Recipe-app-api-proxy

NGINX proxy app for our recipe API

# Usage

# Testing New Account

### Environment variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (Default : `app`)
* `APP_PORT` - Port of the app to forward requests to (Default: `9000`)